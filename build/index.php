<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>WEBSITE_TITLE</title>

    <meta property="og:image" content="/img/media/open-graph.jpg">
    <link rel="shortcut icon" href="/img/media/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="/img/media/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/img/media/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/img/media/apple-touch-icon-114x114.png">

    <!-- Chrome, Firefox OS and Opera -->
    <meta name="theme-color" content="#000">
    <!-- Windows Phone -->
    <meta name="msapplication-navbutton-color" content="#000">
    <!-- iOS Safari -->
    <meta name="apple-mobile-web-app-status-bar-style" content="#000">

    <style>
        <!--=include css/style.css -->
    </style>
</head>
<body>
    <header id="header">
        <img src="/img/pexels-photo-110854.jpg" alt="">
    </header>

    <footer id="footer">© website.com 2017. All rights reserved</footer>

    <script>
        //=require js/jquery.min.js
        //=require js/main.js
    </script>
</body>
</html>
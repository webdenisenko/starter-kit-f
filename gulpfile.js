const gulp = require('gulp'),                       // Preprocessors
    compass = require('gulp-compass'),              // Compass
    babel = require('gulp-babel'),                  // Error notifications
    gutil = require('gulp-util'),                   // Logger
    browserSync = require('browser-sync').create(), // AutoReloader
    tinify = require('gulp-tinify'),                // Image optimization
    gulpNewer = require('gulp-newer'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    mozjpeg = require('imagemin-mozjpeg'),
    imageminSvgo = require('imagemin-svgo'),
    autoprefixer = require('gulp-autoprefixer'),    // CSS optimization
    cleanCSS = require('gulp-clean-css'),
    htmlmin = require('gulp-htmlmin'),              // HTML optimization
    gulpInclude = require('gulp-include'),
    uglify = require('gulp-uglify'),                // JS optimization
    ftp = require( 'vinyl-ftp' ),                   // FTP Deploy
    gulpCopy = require('gulp-copy'),                // Copy
    runSequence = require('run-sequence').use(gulp),// Sequence
    config = require('./config.json');              // Config file


// Preprocessor SASS
function StyleErrors(err) {
    const type = err.type || '';
    const message = err.message || '';
    const extract = err.extract || [];
    const line = err.line || '';
    const column = err.column || '';
    gutil.log(gutil.colors.red.bold('[SASS error]') +' '+ gutil.colors.bgRed(type) +' ('+ line +':'+ column +')');
    gutil.log(gutil.colors.bold('message:') +' '+ message);
    gutil.log(gutil.colors.bold('codeframe:') +'\n'+ extract.join('\n'));
    this.emit('end');
}
gulp.task('sass', function () {
    if( ! config.path.build.sass){
        gutil.log(gutil.colors.red.bold('Warning:') + ' SASS config.path is not set');
        return this.emit('end');
    }

    return gulp.src(config.path.build.sass + '/**/*.+(scss|sass)')
        .pipe(
            compass({
                project: __dirname,
                style: 'expanded',
                css: config.path.build.css,
                sass: config.path.build.sass,
                javascript: config.path.build.js,
                image: config.path.dist.img
            }).on('error', StyleErrors)
        )
        .pipe(gulp.dest(config.path.build.css));
});

// Preprocessor ES6 to ES5 with `browserify`
//
// function JSError(err) {
//     const message = err.message || '';
//     const errName = err.name || '';
//     const codeFrame = err.codeFrame || '';
//     gutil.log(gutil.colors.red.bold('[JS babel error]')+' '+ gutil.colors.bgRed(errName));
//     gutil.log(gutil.colors.bold('message:') +' '+ message);
//     gutil.log(gutil.colors.bold('codeframe:') + '\n' + codeFrame);
//     this.emit('end');
// }
// gulp.task('js', function() {
//     return gulp.src(config.path.build.js + '/**/*.js')
//         .pipe(
//             babel({
//                 presets: ['es2015']
//             }).on('error', JSError)
//         )
//         .pipe(gulp.dest(config.path.dist.js));
// });


// HTML generate
gulp.task('html', function() {
    if( ! config.path.build.root){
        gutil.log(gutil.colors.red.bold('Warning:') + ' Pages config.path is not set');
        return this.emit('end');
    }

    gulp.src(config.path.build.root + '/**/*.+(html|php)')
        .pipe(gulpInclude({
            includePaths: [
                __dirname + "/build"
            ]
        }))
        .on('error', gutil.log)
        .pipe(gulp.dest(config.path.dist.root));

    gutil.log(gutil.colors.green.bold('Success:') + ' HTML optimization');
    return this.emit('end');

});

// LiveReload server
gulp.task('browser-sync', function() {
    return browserSync.init({
        open: false,
        notify: false,
        proxy: config.serverUrl ? config.serverUrl : 'http://'+(__dirname.split("\\")[__dirname.split("\\").length-1])+'/'
    });
});

// AutoSave JS/SASS and LiveReload
gulp.task('watch', ['browser-sync'], function() {

    // SASS to CSS preprocessor
    if(config.path.build.sass) gulp.watch(config.path.build.sass+'/**/*.+(scss|sass)', ['sass']);

    // CSS watch
    if(config.path.build.css) gulp.watch(config.path.build.css+'/**/*.css', ['html'])

    // JS watch
    if(config.path.build.js) gulp.watch(config.path.build.js+'/**/*.js', ['html']);

    // HTML/PHP watch
    if(config.path.build.root) gulp.watch(config.path.build.root+'/**/*.+(php|html)', ['html']);
    if(config.path.dist.root) gulp.watch(config.path.dist.root+'/**/*.+(php|html)', browserSync.reload);

});

// Images optimization
gulp.task('img-o', function() {
    if( ! config.path.build.img){
        gutil.log(gutil.colors.red.bold('Warning:') + ' IMG config.path is not set');
        return this.emit('end');
    }

    gulp.src(config.path.build.img+'/**/*.+(png|jpg|jpeg)')
        .pipe(gulpNewer(config.path.dist.img))
        .pipe(imagemin([
            imagemin.gifsicle({interlaced: true}),
            mozjpeg(),
            pngquant({quality:'85-100'}),
            imagemin.svgo({plugins: [{removeViewBox: true}]})
        ]))
        .pipe(gulp.dest(config.path.dist.img));

    gulp.src(config.path.build.img+'/**/*.svg')
        .pipe(imagemin({
            interlaced: true,
            progressive: true,
            svgoPlugins: [{removeViewBox: false}]
        }))
        .pipe(gulp.dest(config.path.dist.img));

    gutil.log(gutil.colors.green.bold('Success:') + ' IMG optimization');
    return this.emit('end');
});

// CSS optimization
gulp.task('css-o', function() {
    if( ! config.path.dist.css){
        gutil.log(gutil.colors.red.bold('Warning:') + ' CSS config.path is not set');
        return this.emit('end');
    }

    gulp.src(config.path.build.css+'/**/*.css')
        .pipe(autoprefixer('last 15 version'))
        .pipe(cleanCSS())
        .pipe(gulp.dest(config.path.dist.css));

    gutil.log(gutil.colors.green.bold('Success:') + ' CSS optimization');
    return this.emit('end');
});

// JS optimization
gulp.task('js-o', function() {
    if( ! config.path.build.js){
        gutil.log(gutil.colors.red.bold('Warning:') + ' JS config.path is not set');
        return this.emit('end');
    }

    gulp.src(config.path.build.js+'/**/*.js')
        .pipe(uglify())
        .pipe(gulp.dest(config.path.dist.js));

    gutil.log(gutil.colors.green.bold('Success:') + ' JS optimization');
    return this.emit('end');
});

// HTML optimization
gulp.task('html-o', function() {
    if( ! config.path.build.root){
        gutil.log(gutil.colors.red.bold('Warning:') + ' Pages config.path is not set');
        return this.emit('end');
    }

    gulp.src(config.path.build.root + '/**/*.+(html|php)')
        .pipe(gulpInclude({
            includePaths: [
                __dirname + "/dist"
            ]
        }))
        .on('error', gutil.log)
        .pipe(htmlmin({
            collapseWhitespace: true,
        }))
        .pipe(gulp.dest(config.path.dist.root));

    gutil.log(gutil.colors.green.bold('Success:') + ' HTML optimization');
    return this.emit('end');
});

// Optimize and upload
gulp.task('upload', function () {
    const ftpAccess = config.ftpAccess;
    ftpAccess.log = gutil.log;

    const conn = ftp.create(config.ftpAccess);

    // using base = '.' will transfer everything to /public_html correctly
    // turn off buffering in gulp.src for best performance

    return gulp.src( config.path.dist.root+'/**/*', { base: './dist', buffer: false } )
        .pipe( conn.newer( '/' ) ) // only upload newer files
        .pipe( conn.dest( '/' ) );

    gutil.log(gutil.colors.green.bold('Success:') + ' Uploaded');
    return this.emit('end');
});

// Optimize and upload PART #1
gulp.task('deploy1', ['css-o', 'js-o', 'img-o']);
// Optimize and upload PART #2
gulp.task('deploy2', ['html-o', 'upload']);

gulp.task('default', ['watch']);